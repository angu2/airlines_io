import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.application.Application;

import static javafx.application.Application.launch;

public class Main extends Application {
    public static void main(String[] args) throws Exception {
        Application.launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/GUI/FasadaMain.fxml"));
//        root = FXMLLoader.load()
        //
        Scene scene = new Scene(root);
//
        stage.setScene(scene);
        stage.show();
    }

}


