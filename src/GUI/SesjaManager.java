package GUI;

import airlines.Bilet;
import airlines.Pasazer;
import airlines.Users.KontrolerManager;
import airlines.Users.Manager;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SesjaManager extends Application {

    @FXML
    Button buttonWyszukajPasazerow;
    @FXML
    Button buttonWyszukaj;
    @FXML
    Label labelName;
    @FXML
    Label labelSurname;
    @FXML
    Label labelId;
    @FXML
    TextField textFieldName;
    @FXML
    TextField textFieldSurname;
    @FXML
    TextField textFieldId;
    @FXML
    Label labelLoginAs;

    @FXML
    Label labelPassengerId;
    @FXML
    Label labelPassengerName;
    @FXML
    Label labelPassengerSurname;
    @FXML
    Label labelPassengerIdNr;
    @FXML
    Label labelPassengerBirthDate;

    @FXML
    Button buttonPokazBilet;
    @FXML
    Label labelTicketId;
    @FXML
    Label labelTicketPrice;
    @FXML
    Label labelTicketPayed;
    @FXML
    Label labelTicketDeparturePlace;
    @FXML
    Label labelTicektArrivalPlace;
    @FXML
    Label labelTicektdepartureDate;
    @FXML
    Label labelTicketArrivalDate;


    @FXML
    TextField textFieldModifyTicketPrice;
    @FXML
    CheckBox checkboxModifyTicketPaid;


    Manager manager;
    KontrolerManager kontrolerManager;
    Bilet bilet;
    Pasazer pasazer;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

    }

    public void InitData(Manager manager) {
        this.manager = manager;
        labelLoginAs.textProperty().setValue("Jesteś zalogowany jako: " + manager.getLogin());

        // Stworzenie kontrolera Manager
        kontrolerManager = new KontrolerManager();
    }

    @FXML
    public void Wyszukaj(ActionEvent event) throws Exception {
        buttonWyszukajPasazerow.setVisible(false);
        buttonWyszukaj.setVisible(true);
        textFieldName.setVisible(true);
        textFieldSurname.setVisible(true);
        textFieldId.setVisible(true);
        labelName.setVisible(true);
        labelSurname.setVisible(true);
        labelId.setVisible(true);
    }

    @FXML
    public void WpisanieDanych(ActionEvent event) throws Exception {
        if (textFieldName.textProperty().getValue().equals("") && textFieldId.textProperty().getValue().equals("") && textFieldSurname.textProperty().getValue().equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Podaj przynajmniej jeden parametr");
            alert.showAndWait();
        } else {
            kontrolerManager.WyszukiwaniePasazerow(this);
        }
    }


    @FXML
    public List<String> PodajDane() {
        List<String> dataList = new ArrayList<>();

        dataList.add(textFieldName.textProperty().getValue());
        dataList.add(textFieldSurname.textProperty().getValue());
        dataList.add(textFieldId.textProperty().getValue());
        return dataList;
    }

    @FXML
    public void WyswietlBilet(ActionEvent event) throws IOException {

        FXMLLoader myLoader = new FXMLLoader(getClass().getResource("BiletWidok.fxml"));
        myLoader.setController(this);
        Pane root = (Pane) myLoader.load();

        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Bilet");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.show();

        labelTicektArrivalPlace.textProperty().setValue(bilet.getLot().getMiejscePrzylotu().getMiejscowosc());
        labelTicketArrivalDate.textProperty().setValue(bilet.getLot().getDataPrzylotu());
        labelTicketDeparturePlace.textProperty().setValue(bilet.getLot().getMiejsceWylotu().getMiejscowosc());
        labelTicektdepartureDate.textProperty().setValue(bilet.getLot().getDataWylotu());
        labelTicketId.textProperty().setValue(String.valueOf(bilet.getId()));
        if (bilet.isOplacony()) {
            labelTicketPayed.textProperty().setValue("Tak");
        } else {
            labelTicketPayed.textProperty().setValue("Nie");
        }
        labelTicketPrice.textProperty().setValue(String.valueOf(bilet.getCena()));
    }

    @FXML
    public void ModyfikujBilet(ActionEvent event) throws IOException {

        ((Node) event.getSource()).getScene().getWindow().hide();

        FXMLLoader myLoader = new FXMLLoader(getClass().getResource("ModyfikujBiletWidok.fxml"));
        myLoader.setController(this);
        Pane root = (Pane) myLoader.load();

        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Bilet");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.show();


        textFieldModifyTicketPrice.textProperty().setValue(String.valueOf(bilet.getCena()));
        checkboxModifyTicketPaid.selectedProperty().setValue(bilet.isOplacony());

    }

    @FXML
    public void ZatwierdzZmiany(ActionEvent event) throws Exception {
        ((Node) event.getSource()).getScene().getWindow().hide();

        bilet = kontrolerManager.ZmianaBiletu(bilet, Float.parseFloat(textFieldModifyTicketPrice.textProperty().getValue()), checkboxModifyTicketPaid.isSelected());
        this.WyswietlBilet(event);
    }

    @FXML
    public void AnulujBilet(ActionEvent event) throws Exception {
        ((Node) event.getSource()).getScene().getWindow().hide();

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        if (!kontrolerManager.AnulowanieBiletu(bilet)) {
            alert.setContentText("Anulowano bilet");
        } else {
            alert.setContentText("Anulowano bilet i zwrócono pieniądze");
        }
        alert.showAndWait();

        bilet = null;
        buttonPokazBilet.setDisable(true);

    }

    @FXML
    public void KontaktZPasazerem(ActionEvent event) throws Exception {
        FXMLLoader myLoader = new FXMLLoader(getClass().getResource("KontaktWidok.fxml"));
        myLoader.setController(this);
        Pane root = (Pane) myLoader.load();

        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setScene(scene);
        stage.setTitle("Bilet");
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setResizable(false);
        stage.show();

    }

    @FXML
    public void Kontakt(ActionEvent event) throws Exception {
        ((Node) event.getSource()).getScene().getWindow().hide();
        kontrolerManager.Kontakt(pasazer);
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText("Wysłano e-maila");
        alert.showAndWait();
    }


    @FXML
    public void WyswietlPasazera(Pasazer pasazer, Bilet bilet) throws Exception {
        this.pasazer = pasazer;
        this.bilet = bilet;

        if (pasazer != null) {
            FXMLLoader myLoader = new FXMLLoader(getClass().getResource("PasazerWidok.fxml"));
            myLoader.setController(this);
            Pane root = (Pane) myLoader.load();

            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("pasazer");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            stage.show();

            labelPassengerBirthDate.textProperty().setValue(pasazer.getDataUrodzenia());
            labelPassengerId.textProperty().setValue(String.valueOf(pasazer.getId()));
            labelPassengerIdNr.textProperty().setValue(pasazer.getNrDowodu());
            labelPassengerName.textProperty().setValue(pasazer.getImie());
            labelPassengerSurname.textProperty().setValue(pasazer.getNazwisko());
            if (bilet == null) {
                buttonPokazBilet.setDisable(true);
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Nie znaleziono pasażera");
            alert.showAndWait();
        }
    }

    public void Wyjscie(){

    }

    public void KontrolowanieRokladuLotow(){

    }

}
