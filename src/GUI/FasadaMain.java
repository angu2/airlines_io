package GUI;

import airlines.Users.Manager;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javafx.scene.control.*;

import javafx.event.ActionEvent;

public class FasadaMain extends Application {

    @FXML
    Button buttonLoginManager;

    @FXML
    TextField textFieldLoginManager;

    @FXML
    PasswordField textFieldPasswordManager;

    @FXML
    private void Logowanie(ActionEvent event) throws Exception {
        String login = textFieldLoginManager.textProperty().getValue();
        if (login.equals("admin") && textFieldPasswordManager.getText().equals("admin")) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setContentText("Zostałeś poprawnie zalogowany");
            alert.showAndWait();

            // Stworzenie managera z nazwa uzytkownika
            Manager manager = new Manager(login);

            ((Node) event.getSource()).getScene().getWindow().hide();

            FXMLLoader myLoader = new FXMLLoader(getClass().getResource("SesjaManager.fxml"));
            Pane root = (Pane) myLoader.load();
            SesjaManager controller = myLoader.<SesjaManager>getController();

            controller.InitData(manager);

            Scene scene = new Scene (root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setTitle("Sesja Manager");
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            stage.show();

        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setContentText("Nieprawidłowe dane");
            alert.showAndWait();
        }
    }


    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
    }
}
