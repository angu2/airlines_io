package airlines;

public class Lotnisko {

    private int id;
    private String miejscowosc;
    private String strefaCzasowa;
    private int wysokoscNpm;
    private String kodIATA;
    private String kodICAO;
    private int iloscTerminali;

    public Lotnisko(int id, String miejscowosc, String strefaCzasowa, int wysokoscNpm, String kodIATA, String kodICAO, int iloscTerminali) {
        this.id = id;
        this.miejscowosc = miejscowosc;
        this.strefaCzasowa = strefaCzasowa;
        this.wysokoscNpm = wysokoscNpm;
        this.kodIATA = kodIATA;
        this.kodICAO = kodICAO;
        this.iloscTerminali = iloscTerminali;
    }

    public int getId() {
        return id;
    }

    public String getMiejscowosc() {
        return miejscowosc;
    }

    public String getStrefaCzasowa() {
        return strefaCzasowa;
    }

    public int getWysokoscNpm() {
        return wysokoscNpm;
    }

    public String getKodIATA() {
        return kodIATA;
    }

    public String getKodICAO() {
        return kodICAO;
    }

    public int getIloscTerminali() {
        return iloscTerminali;
    }
}
