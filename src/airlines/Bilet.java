package airlines;

public class Bilet {

    private int id;
    private float cena;
    private boolean oplacony = false;


    private Lot lot;
    private Pasazer pasazer;

    public Bilet(int id, float cena, boolean oplacony, Lot lot) {
        this.id = id;
        this.cena = cena;
        this.oplacony = oplacony;
        this.lot = lot;
    }

    public int getId() {
        return id;
    }

    public float getCena() {
        return cena;
    }

    public boolean isOplacony() {
        return oplacony;
    }

    public Lot getLot() {
        return lot;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCena(float cena) {
        this.cena = cena;
    }

    public void setOplacony(boolean oplacony) {
        this.oplacony = oplacony;
    }

    public void setLot(Lot lot) {
        this.lot = lot;
    }

    public void setPasazer(Pasazer pasazer) {
        this.pasazer = pasazer;
    }

    public Pasazer getPasazer() {
        return pasazer;
    }
}
