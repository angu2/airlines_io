package airlines.Users;

import GUI.SesjaManager;
import airlines.Bilet;
import airlines.Pasazer;
import airlines.mysql.KomponentDAO;

import java.util.List;

public class KontrolerManager {

    public static List<String> listaDanych;
    private KomponentDAO komponentDAO;

    public KontrolerManager() {
        komponentDAO = new KomponentDAO();
    }

    public void WyszukiwaniePasazerow(SesjaManager sesjaManager) throws Exception {
        Pasazer pasazer;
        Bilet bilet = null;

        listaDanych = sesjaManager.PodajDane();
        pasazer = komponentDAO.ZnajdzPasazera(listaDanych);

        if (pasazer == null) {
            System.out.println("NULLLLLLLLLLLLLLLLL!");
        } else {
            bilet = komponentDAO.ZnajdzBilet(pasazer.getId());
            if (bilet != null) {
                bilet.setPasazer(pasazer);
            }
        }
        sesjaManager.WyswietlPasazera(pasazer, bilet);
    }

    public Bilet ZmianaBiletu(Bilet bilet, float cena, boolean oplacony) throws Exception {
        bilet.setCena(cena);
        bilet.setOplacony(oplacony);
        komponentDAO.AktualizujBilet(bilet);

        return bilet;
    }

    public boolean AnulowanieBiletu(Bilet bilet) throws Exception {
        komponentDAO.UsunBilet(bilet);
        return this.ZwrotPieniedzy(bilet);
    }

    public boolean ZwrotPieniedzy(Bilet bilet) {
        if (bilet.isOplacony()) {
            String daneDoPrzelewu = bilet.getPasazer().getDaneDoPrzelewu();
            float cena = bilet.getCena();
            this.Bank(daneDoPrzelewu, cena);
            return true;
        } else {
            return false;
        }
    }

    public void Kontakt(Pasazer pasazer) throws Exception {
        String emailPasazer = komponentDAO.GetEmailPasazer(pasazer);
        if (emailPasazer != null) {
            SerwerPocztowy(emailPasazer);
        }
    }

    public void SerwerPocztowy(String email) {
        //WYSYLANIE
    }

    public void Bank(String daneDoPrzelewu, float cena) {
        System.out.println("Wykonano przelew do " + daneDoPrzelewu);
    }

    public void KontrolowanieRozkladuLotow(){

    }

}
