package airlines.Users;

public class Manager {

    public String login;

    public Manager(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
