package airlines;

public class Lot {
    private Lotnisko miejsceWylotu;
    private Lotnisko miejscePrzylotu;
    private int id;
    private String dataWylotu;
    private String dataPrzylotu;
    private float aktualnaCena;

    public Lot(Lotnisko miejsceWylotu, Lotnisko miejscePrzylotu, int id, String dataWylotu, String dataPrzylotu, float aktualnaCena) {
        this.miejsceWylotu = miejsceWylotu;
        this.miejscePrzylotu = miejscePrzylotu;
        this.id = id;
        this.dataWylotu = dataWylotu;
        this.dataPrzylotu = dataPrzylotu;
        this.aktualnaCena = aktualnaCena;
    }

    public Lot(int id, String dataWylotu, String dataPrzylotu, float aktualnaCena) {
        this.id = id;
        this.dataWylotu = dataWylotu;
        this.dataPrzylotu = dataPrzylotu;
        this.aktualnaCena = aktualnaCena;
    }

    public Lotnisko getMiejsceWylotu() {
        return miejsceWylotu;
    }

    public Lotnisko getMiejscePrzylotu() {
        return miejscePrzylotu;
    }

    public int getId() {
        return id;
    }

    public String getDataWylotu() {
        return dataWylotu;
    }

    public String getDataPrzylotu() {
        return dataPrzylotu;
    }

    public float getAktualnaCena() {
        return aktualnaCena;
    }

    public void setMiejsceWylotu(Lotnisko miejsceWylotu) {
        this.miejsceWylotu = miejsceWylotu;
    }

    public void setMiejscePrzylotu(Lotnisko miejscePrzylotu) {
        this.miejscePrzylotu = miejscePrzylotu;
    }
}


