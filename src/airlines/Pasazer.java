package airlines;

public class Pasazer {

    private int id;
    private String imie;
    private String nazwisko;
    private String dataUrodzenia;
    private String nrDowodu;

    public Pasazer(int id, String imie, String nazwisko, String dataUrodzenia, String nrDowodu) {
        this.id = id;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
        this.nrDowodu = nrDowodu;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public String getDataUrodzenia() {
        return dataUrodzenia;
    }

    public void setDataUrodzenia(String dataUrodzenia) {
        this.dataUrodzenia = dataUrodzenia;
    }

    public String getNrDowodu() {
        return nrDowodu;
    }

    public void setNrDowodu(String nrDowodu) {
        this.nrDowodu = nrDowodu;
    }

    public String getDaneDoPrzelewu() {
        return this.getImie() + " " + this.getNazwisko();
    }

}
