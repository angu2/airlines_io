package airlines.mysql;

import airlines.Bilet;
import airlines.Lot;
import airlines.Lotnisko;
import airlines.Pasazer;

import java.sql.*;
import java.util.List;

public class KomponentDAO {
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;

    public void CreateConnection() throws SQLException, ClassNotFoundException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/airline?user=root&password=root&useLegacyDatetimeCode=false&serverTimezone=UTC");
        } catch (Exception e) {
            throw e;
        }
    }

    public void UsunBilet(Bilet bilet) throws Exception {
        ResultSet resultSet = null;
        try {

            this.CreateConnection();

            System.out.print("Usuwam bilet o id: ");
            System.out.println(bilet.getId());

            preparedStatement = connect
                    .prepareStatement("DELETE FROM ticket WHERE ticket_id = ?");
            preparedStatement.setInt(1, bilet.getId());
            preparedStatement.executeUpdate();

        } catch (
                Exception e) {
            throw e;
        } finally {
            Close(resultSet);
        }

    }

    public void AktualizujBilet(Bilet bilet) throws Exception {
        ResultSet resultSet = null;
        try {

            this.CreateConnection();

            preparedStatement = connect
                    .prepareStatement("update ticket set price = ?, paid=? WHERE ticket_id = ?");

            // Parameters start with 1
            preparedStatement.setFloat(1, bilet.getCena());
            preparedStatement.setBoolean(2, bilet.isOplacony());
            preparedStatement.setInt(3, bilet.getId());

            preparedStatement.executeUpdate();

        } catch (Exception e) {
            throw e;
        } finally {
            Close(resultSet);
        }
    }

    public Pasazer ZnajdzPasazera(List<String> dataList) throws Exception {
        ResultSet resultSet = null;
        try {

            this.CreateConnection();

            statement = connect.createStatement();
            String firstname = dataList.get(0);
            String lastname = dataList.get(1);
            String id_number = dataList.get(2);

            String sql = "SELECT * FROM passanger_view WHERE ";
            sql += "firstname LIKE '%" + firstname + "%'";
            sql += " AND lastname LIKE '%" + lastname + "%'";
            sql += " AND id_number LIKE '%" + id_number + "%'";
            resultSet = statement.executeQuery(sql);

            int count = 0;
            while (resultSet.next()) {
                count++;
            }

            if (count == 1) {
                resultSet.first();
                return new Pasazer(resultSet.getInt("passenger_id"), resultSet.getString("firstname"), resultSet.getString("lastname"), resultSet.getDate("birthdate").toString(), resultSet.getString("id_number"));
            } else {
                return null;
            }

        } catch (Exception e) {
            throw e;
        } finally {
            Close(resultSet);
        }
    }

    public String GetEmailPasazer(Pasazer pasazer) throws Exception {
        ResultSet resultSet = null;
        try {

            this.CreateConnection();

            statement = connect.createStatement();

            String sql = "SELECT * FROM ticket WHERE passenger_id = " + pasazer.getId() + " LIMIT 1";
            resultSet = statement.executeQuery(sql);

            if (resultSet.first()) {
                int idOwner = resultSet.getInt("owner_id");
                sql = "SELECT * FROM user WHERE user_id = " + idOwner + " LIMIT 1";
                resultSet = statement.executeQuery(sql);
                if (resultSet.first()) {
                    return resultSet.getString("email");
                }
            }

            return null;

        } catch (Exception e) {
            throw e;
        } finally {
            Close(resultSet);
        }

    }

    public Bilet ZnajdzBilet(int idPasazera) throws Exception {
        ResultSet resultSet = null;
        try {

            this.CreateConnection();

            statement = connect.createStatement();

            String sql = "SELECT * FROM ticket WHERE passenger_id = " + idPasazera + " LIMIT 1";
            resultSet = statement.executeQuery(sql);

            if (resultSet.first()) {
                System.out.println(resultSet.getInt("flight_id"));
                ZnajdzLot(resultSet.getInt("flight_id"));
                Lot lot = ZnajdzLot(resultSet.getInt("flight_id"));
                return new Bilet(resultSet.getInt("ticket_id"), resultSet.getFloat("price"), true, lot);
            } else {
                return null;
            }


        } catch (Exception e) {
            throw e;
        } finally {
            Close(resultSet);
        }
    }

    public Lot ZnajdzLot(int idLotu) throws Exception {
        ResultSet resultSet = null;
        try {

            this.CreateConnection();

            statement = connect.createStatement();

            String sql = "SELECT * FROM flight WHERE flight_id = " + idLotu + " LIMIT 1";
            resultSet = statement.executeQuery(sql);

            if (resultSet.first()) {
                System.out.println(resultSet.getInt("airport_arrival_id"));

                Lot lot = new Lot(resultSet.getInt("flight_id"), resultSet.getTimestamp("date_depature").toString(), resultSet.getTimestamp("date_arrival").toString(), resultSet.getFloat("price"));

//                //miejsceWylotu
                Lotnisko miejsceWylotu = ZnajdzLotnisko(resultSet.getInt("airport_departure_id"));
                lot.setMiejsceWylotu(miejsceWylotu);
//
//                //miejscePrzylotu
                Lotnisko miejscePrzylotu = ZnajdzLotnisko(resultSet.getInt("airport_arrival_id"));
                lot.setMiejscePrzylotu(miejscePrzylotu);

                return lot;
            } else {
                return null;
            }


        } catch (Exception e) {
            throw e;
        } finally {
            Close(resultSet);
        }
    }

    public Lotnisko ZnajdzLotnisko(int idLotniska) throws Exception {
        ResultSet resultSet = null;
        try {


            this.CreateConnection();

            statement = connect.createStatement();

            String sql = "SELECT * FROM airport WHERE airport_id = " + idLotniska + " LIMIT 1";
            resultSet = statement.executeQuery(sql);

            if (resultSet.first() != false) {
                System.out.println(resultSet.getString("city"));
                return new Lotnisko(resultSet.getInt("airport_id"), resultSet.getString("city"), resultSet.getString("time_zone"), resultSet.getInt("sea_level"), resultSet.getString("code_iata"), resultSet.getString("code_icao"), resultSet.getInt("terminals_quantity"));
            } else {
                return null;
            }


        } catch (Exception e) {
            throw e;
        } finally {
            Close(resultSet);
        }
    }

    // You need to Close the resultSet
    private void Close(ResultSet resultSet) {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }

}

