package airlines;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class BiletTest {

    @Test
    public void getId() {
        Bilet bilet = new Bilet(1, (float) 20.0, true, null);
        Assert.assertEquals(1, bilet.getId());
        Assert.assertEquals(10, bilet.getId());
    }

    @Test
    public void isOplacony() {
        Bilet bilet = new Bilet(1, (float) 20.0, true, null);
        bilet.setOplacony(false);
        Assert.assertEquals(false, bilet.isOplacony());
    }
}