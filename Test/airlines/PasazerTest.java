package airlines;

import airlines.Users.KontrolerManager;
import airlines.mysql.KomponentDAO;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PasazerTest {

    @Test
    public void getId() {
        Pasazer pasazer = new Pasazer(1, "Szymon", "Korda", "200", "300");
        Assert.assertEquals(1, pasazer.getId());
        Assert.assertNotEquals(10, pasazer.getId());
    }

    @Test
    public void ZnajdzPasazera() throws Exception {
        KomponentDAO komponent = new KomponentDAO();
        List<String> dataList = new ArrayList<>();
        dataList.add("Maksymilian");
        dataList.add("Wrona");
        dataList.add("CFE 85236");
        assertNull(komponent.ZnajdzPasazera(dataList));
        dataList.clear();
        dataList.add("Maksymilian");
        dataList.add("Pawlak");
        dataList.add("CFE 85236");
        assertEquals("Pawlak", komponent.ZnajdzPasazera(dataList).getNazwisko());
}
    @Test
    public void ZmianaBiletu() throws Exception {
        Bilet bilet = new Bilet(4, (float) 620.50, false, null);
        KontrolerManager kontroler = new KontrolerManager();
        kontroler.ZmianaBiletu(bilet, 300, true);
        KomponentDAO komponent = new KomponentDAO();
        assertEquals(300, komponent.ZnajdzBilet(1).getCena(), 0);
        assertEquals(4, komponent.ZnajdzBilet(1).getId());
        assertTrue(komponent.ZnajdzBilet(1).isOplacony());
}




}